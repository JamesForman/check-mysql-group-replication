#!/usr/bin/python3

'''
Script to check the state of MySQL Group Replication.

Must be provided with the expected number of servers in the group with --count and database connection details.

Only the SELECT permission on performance_schema.replication_group_members is required.

James Forman <james@jamesforman.co.nz>.

Version: 1.0.0
'''

import argparse
import sys
import pymysql.cursors

STATE_OK = 0
STATE_WARNING = 1
STATE_CRITICAL = 2
STATE_UNKNOWN = 3

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Get check values')
    parser.add_argument('--count', help='Number of servers expected to be in group')
    parser.add_argument('--hostname', help='The MySQL Hostname')
    parser.add_argument('--username', help='The MySQL Username')
    parser.add_argument('--password', help='The MySQL Password')
    args = parser.parse_args()

    # Connect to the database
    connection = pymysql.connect(host=args.hostname,
                                 user=args.username,
                                 password=args.password,
                                 db='performance_schema',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `performance_schema`.`replication_group_members`;"
            cursor.execute(sql)
            result = cursor.fetchall()

            # Check if only a single member is in a group larger than 2 members
            if int(args.count) >= 2 and int(cursor.rowcount) == 1:
                print("CRITICAL: Member appears to have left the group")
                sys.exit(STATE_CRITICAL)
            # Check if there are less than the expected number of members
            elif int(cursor.rowcount) < int(args.count):
                print("CRITICAL: There are too few members in the group ({0}/{1})".format(cursor.rowcount, args.count))
                sys.exit(STATE_CRITICAL)
            # Check if there are too many members in the group
            elif int(cursor.rowcount) > int(args.count):
                print("UNKNOWN: There are more members than expected in the group ({0}/{1})".format(cursor.rowcount, args.count))

            count_ok = 0
            count_recovering = 0
            count_bad = 0

            for server in result:
                if server['MEMBER_STATE'] == 'ONLINE':
                    count_ok = count_ok + 1
                elif server['MEMBER_STATE'] == 'RECOVERING':
                    # So that we can see what server(s) are recovering in the check output
                    print("{0} is recovering".format(server['MEMBER_HOST']))
                    count_recovering = count_recovering + 1
                else:
                    print("{0} is in a bad state".format(server['MEMBER_HOST']))
                    count_bad = count_bad + 1

            if int(count_ok) == int(args.count):
                print("OK: All the servers are ONLINE ({0}/{1})".format(count_ok, args.count))
                sys.exit(STATE_OK)
            # If all the member are recovering the group is going to be in super-read-only
            elif int(count_recovering) == int(cursor.rowcount):
                print("CRITICAL: All members are recovering, group is broken")
                sys.exit(STATE_CRITICAL)
            # Warning if any of the servers are recovering, but not bad
            elif int(count_recovering) != 0 and int(count_bad) == 0:
                print("WARNING: Servers are recovering")
                sys.exit(STATE_WARNING)
            # Critical for anything not matched above
            else:
                print("CRITICAL: Some of the servers are in a bad state")
                sys.exit(STATE_CRITICAL)

    finally:
        connection.close()
