<!--alex disable host -->
# Purpose

This script is used to monitor all hosts that are part of MySQL Group Replication. It requires that:

* one check be created per host
* a MySQL user be created that grants only the SELECT permission on performance_schema.replication_group_members

# Requirements and Installation

## Requirements

* Python3
* PyMySQL

## Installaton on Ubuntu

```bash
sudo apt update && sudo apt install python3 python-pymysql
```

# How to use the script

Create a MySQL user to be used by the script and grant the SELECT permission on performance_schema.replication_group_members.

Run the script from each member host.

## Arguments

### Count (required)
```bash
--count
```

The number of servers expected to be in the group.

### Hostname (required)
```bash
--hostname
```

The MySQL host to connect to.

### Username (required)
```bash
--username
```

The MySQL username to authenticate with.

### Password (required)
```bash
--password
```

The MySQL password to authenticate with.

## Example Icinga2 configuration
```
object CheckCommand "mysql_group_replication" {
        import "plugin-check-command"
        command = [ PluginContribDir + "/check_mysql_group_replication.py" ]

    arguments = {
        "--count" = {
            value = "$mysqlgr_count$"
            description = "Number of nodes expected in group"
            required = true
        }
        "--hostname" = {
            value = "$mysqlgr_hostname$"
            description = "Hostname of the MySQL server"
            required = true
        }
        "--username" = {
            value = "$mysqlgr_username$"
            description = "Username of the MySQL user"
            required = true
        }
        "--password" = {
            value = "$mysqlgr_password$"
            description = "Password of the MySQL user"
            required = true
        }
    }
}
```

# Contributing
Please open an issue, create a new branch, complete your work in the branch and open a merge request.
<!--alex enable host -->
